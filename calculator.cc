#include<iostream>
#include<vector>
#include<string>
#include<memory>
using namespace std;

// ast tree node
class TreeNode {
	friend class Calc;
private:
	string token;
	shared_ptr<TreeNode> left;
	shared_ptr<TreeNode> right;

public:
	int compute();
	void printTree();	// debug purpose

	TreeNode() : left(nullptr), right(nullptr) {}
	TreeNode(const string &s) : token(s) {}
};

void TreeNode::printTree() {
	cout << token << " ";
	if(left)
		left->printTree();
	if(right)
		right->printTree();
}

int TreeNode::compute() {
	if(left == nullptr && right == nullptr) {
		return stoi(token);
	}
	if(!left || !right) {
		cout << "missing components" << endl;
		abort();
	}
	int l = left->compute();
	int r = right->compute();
	if(token == "+") return l + r;
	else if(token == "-") return l - r;
	else if(token == "*") return l * r;
	else if(token == "/") return l / r;
	else {
		cout << "unrecognized op: " << token << endl;
		abort();
	}
}

// calculator class
class Calc {
private:
	enum PRE {LOW_PRECEDENCE, HIGH_PRECEDENCE};
	unsigned idx;
	vector<string> tokens;
	shared_ptr<TreeNode> tree;
	void tokenize(const string &, vector<string> &);
	shared_ptr<TreeNode> parse_expr();	// the grammar is [expr][op](expr)[op][expr]...
	shared_ptr<TreeNode> parse_binary_op(shared_ptr<TreeNode>, enum PRE);
public:
	void init(const string &);
	void reset();
	shared_ptr<TreeNode> parse();
	Calc() : idx(0), tree(nullptr) {}
	
};

void Calc::init(const string &s) {
	tokenize(s, tokens);
	tree = shared_ptr<TreeNode>(new TreeNode("0"));
}
shared_ptr<TreeNode> Calc::parse() {
	while(idx < tokens.size()){
		parse_expr();
	}
	return tree;
}

shared_ptr<TreeNode> Calc::parse_expr() {
	while(1) {
		if(idx >= tokens.size()) break;

		string tkn = tokens[idx];
		if(tkn == ")")	break;	// (1+2) may lead to here
		if(tkn == "(") {
			idx++;
			tree = parse_expr();
			// check if () is closed
			if(idx < tokens.size() && tokens[idx] != ")") {
				cout << "unclosed ()" << endl;
				abort();
			}
			idx++;
		} else if(tokens[idx] == "+" || tokens[idx] == "-") {
			shared_ptr<TreeNode> lhs = tree;
			tree = parse_binary_op(lhs, LOW_PRECEDENCE);
		} else if(tokens[idx] == "*" || tokens[idx] == "/") {
			shared_ptr<TreeNode> lhs = tree;
			tree = parse_binary_op(lhs, HIGH_PRECEDENCE);
		} else {
			tree = shared_ptr<TreeNode>(new TreeNode(tkn));
			++idx;
		}
		
	}
	return tree;
}

shared_ptr<TreeNode> 
Calc::parse_binary_op(shared_ptr<TreeNode> lhs, enum PRE pre) {
	shared_ptr<TreeNode> op(new TreeNode(tokens[idx++]));
	if(idx >= tokens.size()) {
		cout << "missing numbers after: " << op->token << endl;
		abort();
	}
	op->left = lhs;
	shared_ptr<TreeNode> rhs = parse_expr();
	op->right = rhs;
	if(pre == HIGH_PRECEDENCE) {
		return op;
	} else if (idx < tokens.size() && 
			   (tokens[idx] == "*" || tokens[idx] == "/")){
		rhs = parse_expr();
		op->right = rhs;
	}
	return op;
}

void Calc::reset() {
	tokens.clear();
	idx = 0;
	tree.reset();
}
void Calc::tokenize(const string &s, vector<string> &tokens) {
	unsigned i = 0;
	while( i < s.size() ) {
		if(isspace(s[i])){
			i++;
		}
		else if(isdigit(s[i])){
			int j = i;
			while(isdigit(s[++j])) ;
			tokens.push_back(s.substr(i, j-i));
			i = j;
		}
		else {
			tokens.push_back(s.substr(i, 1));
			i++;
		}
	}
}


int main() {
	string t;
	Calc c;
	while(1) {
		cout << "$: ";
		getline(cin, t);
		c.init(t);
		cout << "=" << endl;
		cout << "\t" << c.parse()->compute() << endl;
		c.reset();
	}
}


